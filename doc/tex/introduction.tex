
\chapter{Introduction}
\label{ch:introduction} % For referencing the chapter elsewhere, use \autoref{ch:introduction} 

%----------------------------------------------------------------------------------------
The current version of the Bayesian inference package \ares{} (Algorithm for REconstruction and Sampling)
aims at several tasks:
\begin{enumerate}
\item \textbf{Three dimensional (\(3\)D) Large Scale Structure (LSS) inference:}\\
The algorithm aims at inferring the \(3\)D density field, in the linear and non-linear regime, from galaxy observations,
subject to a variety of statistical and systematic uncertainties, such as noise, cosmic variance, selection effects and biases as well as survey geometries. 
\marginpar{\myTitle \myVersion}
\item \textbf{Quantification of joint and non-linear uncertainties:}\\
In order to perform precision inference of the density field in the non-linear regime, the \ares{} algorithm not only accounts for observational stochastic and systematic uncertainties as well as the non-linear relation between data and signal, but also provides a means to thoroughly quantify and propagate all joint and correlated uncertainties.  
\item \textbf{Improving accuracy of radial galaxy positions:}\\
A particular target of the {\small \bf{HADES}} algorithm is the treatment of uncertain radial positions of galaxies
along the Line Of Sight (LOS) as encountered in present and upcoming photometric redshift surveys.
As an outcome, the {\small \bf{HADES}} algorithm provides accurate galaxy redshifts as well as thorough quantification
of corresponding uncertainties.  
\end{enumerate}
These tasks are addressed via a sophisticated multi-million dimensional Markov Chain Monte Carlo algorithm,
providing numerical representations of target LSS posterior distributions.  In exploring the full joint parameter space of the three dimensional density field and possibly also of radial locations of galaxies, the algorithm
accounts for stochastic uncertainties, such as noise, cosmic variance and redshift uncertainties, as well as for specific survey characteristics such as survey geometry and selection effects. Besides these observational effects,
the algorithm also accounts for the non-linear behavior of the gravitationally evolved density field and the non-linear relation between the signal and the data. This is achieved via efficient exploration of a corresponding log-normal Poissonian posterior distribution.
In addition, the algorithm treats the individual redshift uncertainties of millions of galaxies and provides
accurate uncertainty information by providing the corresponding redshift posterior distributions.   

This high dimensional Markov sampler comes in a well wrapped black box, which gives the user
uncomplicated access to the sophisticated Bayesian inference framework represented by the \ares{} algorithm.
The main references describing the numerical, algorithmic and application aspects are \citet{JASCHE2010A}, \citet{JASCHE2010B}, \citet{JASCHEPHOTOZ}, \citet{JascheLavaux15}. 
We recommend reading these papers.

The code is written in ANSI C/C++ and runs on parallel shared memory platforms supporting OpenMP.
\ares{} was written by Jens Jasche and is made available under the License agreement printed
in chapter \autoref{ch:License}
