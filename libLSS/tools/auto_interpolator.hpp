/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tools/auto_interpolator.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2016-2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_TOOLS_AUTO_INTERP_HPP
#define __LIBLSS_TOOLS_AUTO_INTERP_HPP

#include <boost/multi_array.hpp>
#include <cmath>

namespace LibLSS {

  namespace internal_auto_interp {

    template<typename T>
    class auto_interpolator {
    public:
      typedef T bare_t;
      typedef boost::multi_array<T,1> array_type;
    private:
      array_type *array_vals;
      size_t N;
      T start, end, step, overflow, underflow;
    public:

      explicit auto_interpolator(const T& _start, const T& _end, const T& _step,
                      const T& _under, const T& _over,
                      array_type *value)
       : array_vals(value), start(_start), end(_end), step(_step),
         underflow(_under), overflow(_over), N(value->size()) {}

      auto_interpolator(auto_interpolator<T>&& other)
       : array_vals(other.array_vals), start(other.start), end(other.end),
         step(other.step), underflow(other.underflow), overflow(other.overflow),
         N(other.N) {
        other.array_vals = 0;
      }

      ~auto_interpolator() {
        if (array_vals)
          delete array_vals;
      }

      T operator()(const T& a) const {
        T normed_pos = (a-start)/step; 
        T f_pos = std::floor(normed_pos);
        ssize_t i_pos = ssize_t(f_pos);
        T r = normed_pos-f_pos;

        if (i_pos < 0)
          return underflow;
        if (i_pos >= (N-1))
          return overflow;
        return (1-r) * (*array_vals)[i_pos] + r * (*array_vals)[i_pos+1];
      }
    };

    template<typename T, typename Functor>
    auto_interpolator<T> build_auto_interpolator(const Functor& f, const T& start, const T& end, const T& step, const T& underflow, const T& overflow) {
      typedef auto_interpolator<T> a_interp;
      typedef typename a_interp::array_type array_type;
      size_t N = size_t(round((end-start)/step));
      array_type *vals = new array_type(boost::extents[N]);

#pragma omp parallel
      for (size_t i = 0; i < N; i++) {
        T x = start + i * step;
        (*vals)[i] = f(x);
      }

      return a_interp(start, end, step, underflow, overflow, vals);
    }

  }

  using internal_auto_interp::build_auto_interpolator;
  using internal_auto_interp::auto_interpolator;

}


#endif
