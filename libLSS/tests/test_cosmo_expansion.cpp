/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tests/test_cosmo_expansion.cpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2017-2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#include <iostream>
#include "libLSS/physics/cosmo.hpp"

using namespace LibLSS;

int main()
{
    CosmologicalParameters params;
    
    params.omega_r = 0;
    params.omega_k = 0;
    params.omega_m = 0.10;
    params.omega_b = 0.049;
    params.omega_q = 0.90;
    params.w = -1;
    params.wprime = 0;
    params.n_s = 1;
    params.sigma8 = 0.8;
    params.rsmooth = 0;
    params.h = 0.7;
    params.beta = 0;
    params.z0 = 0;
    params.a0 = 1;
    
    
    Cosmology cosmo(params);
    
    for (int i = 0; i <= 100; i++) {
        double z = i/100., znew;
        double d;
        bool pass;
        
        d = cosmo.com2comph(cosmo.a2com(cosmo.z2a(z)));
        znew = cosmo.a2z(cosmo.com2a(cosmo.comph2com(d)));
    
        pass = std::abs(z-znew) < 1e-5;
    
        std::cout << z << " " << d << " " << pass << std::endl;
    }   
    return 0;
}
