/*+
    ARES/HADES/BORG Package -- -- ./libLSS/tests/test_proj.cpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#include <H5Cpp.h>
#include "libLSS/mpi/generic_mpi.hpp"
#include "libLSS/data/spectro_gals.hpp"
#include "libLSS/data/projection.hpp"
#include "libLSS/data/galaxies.hpp"

using namespace LibLSS;

int main(int argc, char **argv)
{
    MPI_Communication *comm = setupMPI(argc, argv);
    H5::H5File f("toto.h5", H5F_ACC_TRUNC);
    typedef GalaxySurvey<NoSelection, BaseGalaxyDescriptor> SurveyType;

    SurveyType survey;
    SurveyType::GalaxyType galaxy;

    galaxy.id = 0;
    galaxy.phi = 1.0;
    galaxy.theta = 0.1;
    galaxy.r = 10.;
    galaxy.zo = 1000;

    survey.addGalaxy(galaxy);

    survey.save(f);
    survey.restoreMain(f);
    return 0;
}

