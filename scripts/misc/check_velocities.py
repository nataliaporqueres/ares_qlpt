#+
#   ARES/HADES/BORG Package -- -- ./scripts/misc/check_velocities.py
#   Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
#   Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>
#
#   Additional contributions from:
#      Guilhem Lavaux <guilhem.lavaux@iap.fr> (2016, 2018)
#      elsner <f.elsner@mpa-garching.mpg.de> (2017)
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of either the CeCILL license or the GNU General Public
#   license, as included with the software package.
#
#   The text of the license is located in Licence_CeCILL_V2.1-en.txt
#   and GPL.txt in the root directory of the source package.
#+
from read_all_h5 import *
import pylab as plt

g=read_all_h5('dump_velocities.h5')


V = g.scalars.L0[0]*g.scalars.L1[0]*g.scalars.L2[0]

q = g.scalars.k_pos_test
H=100.
D=1.
a=1.
f=g.scalars.cosmology['omega_m']**(5./9)
vref = 2* q/((q**2).sum()) / V * g.scalars.A_k_test * f * H * a**2 * D
vborg = g.scalars.lpt_vel[:,::].max(axis=0)

print "vref = %r" % vref
print "vborg = %r" % vborg

print "ratio = %r" % (vborg/vref)
