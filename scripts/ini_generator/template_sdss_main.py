#+
#   ARES/HADES/BORG Package -- -- ./scripts/ini_generator/template_sdss_main.py
#   Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
#   Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>
#
#   Additional contributions from:
#      Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015, 2018)
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of either the CeCILL license or the GNU General Public
#   license, as included with the software package.
#
#   The text of the license is located in Licence_CeCILL_V2.1-en.txt
#   and GPL.txt in the root directory of the source package.
#+
CONFIG=dict(
  absmag_min=-23.,absmag_max = -17.,
  num_subcat=6,catalog='sdss_ares_cat.txt',
  mask='SDSSDR7MASK_4096.fits', ref_subcat=0,
  cutter='magnitude'
  )

radial_selection = 'schechter'
schechter_mstar = -20.44
schechter_alpha = -1.05
schechter_sampling_rate = 2000
schechter_dmax = 1000
bias = 1
nmean = 1
galaxy_bright_apparent_magnitude_cut = 13.5
galaxy_faint_apparent_magnitude_cut = 17.6
