FILE(WRITE ${SRC_DIR}/tools/build/src/user-config.jam "using ${TOOLSET} : cmake : \"${COMPILER}\" ;\n")

execute_process(
  COMMAND "${SRC_DIR}/bootstrap.sh" --prefix=${INSTALL_PATH} toolset=${TOOLSET}-cmake
  RESULT_VARIABLE okcode
)

IF (NOT "${okcode}" STREQUAL "0")
  MESSAGE(FATAL_ERROR "Cannot execute configure command")
ENDIF()
