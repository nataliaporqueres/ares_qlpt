/*+
    ARES/HADES/BORG Package -- -- ./src/common/mock_gen.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2018)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __ARES_MOCK_DATA_HPP
#define __ARES_MOCK_DATA_HPP

#include <CosmoTool/algo.hpp>
#include <CosmoTool/cosmopower.hpp>
#include "libLSS/tools/console.hpp"
#include "libLSS/tools/log_traits.hpp"
#include <boost/format.hpp>
#include "configuration.hpp"
#include "libLSS/physics/cosmo_power.hpp"

#include SAMPLER_MOCK_GENERATOR

#endif
