/*+
    ARES/HADES/BORG Package -- -- ./src/common/preparation_simulation.hpp
    Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
    Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>

    Additional contributions from:
       Florian Führer <fuhrer@iap.fr> (2018)
       Guilhem Lavaux <guilhem.lavaux@iap.fr> (2018)
       Minh Nguyen <minh@mpa-garching.mpg.de> (2017)

    This program is free software; you can redistribute it and/or modify it
    under the terms of either the CeCILL license or the GNU General Public
    license, as included with the software package.

    The text of the license is located in Licence_CeCILL_V2.1-en.txt
    and GPL.txt in the root directory of the source package.

+*/
#ifndef __LIBLSS_ARES_PREPARATION_SIMULATION_HPP
#define __LIBLSS_ARES_PREPARATION_SIMULATION_HPP

#include "libLSS/tools/console.hpp"
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include "libLSS/tools/ptree_translators.hpp"
#include <boost/algorithm/string.hpp>
#include "libLSS/data/spectro_gals.hpp"
#include "libLSS/data/galaxies.hpp"
#include "libLSS/data/survey_load_txt.hpp"
#include "libLSS/data/survey_load_bin.hpp"
#include "libLSS/data/projection.hpp"
#include "libLSS/data/schechter_completeness.hpp"
#include "survey_cutters.hpp"
#include <CosmoTool/interpolate.hpp>
#include "libLSS/tools/ptree_vectors.hpp"
#include "libLSS/tools/fused_array.hpp"
#include "libLSS/tools/fused_assign.hpp"
#include "preparation.hpp"
#include<boost/tokenizer.hpp>
#include<string>

namespace LibLSS_prepare {

	using namespace LibLSS;

	typedef boost::multi_array_types::extent_range range;

    typedef RandomNumberMPI<GSL_RandomNumber> RGenType;
    typedef ScalarStateElement<GalaxySampleSelection> InfoSampleSelection;

		static GalaxySurveyType& getHaloCatalog(MarkovState& state, size_t cat_idx)
		{
			return state.get<GalaxyElement>(str(format("halo_catalog_%d") % cat_idx))->get();
		}

		template<typename Function>
    SurveyPreparer resolveHaloSurvey(MarkovState& state, Function f)
    {
      return SurveyPreparer([f,&state](size_t cat_idx,
             ArrayType::ArrayType& grid,
             size_t * const& N,
             double * const& corner,
             double * const& L,
             double * const& delta)->size_t {
							 //return 0;
        return f(
					getHaloCatalog(state, cat_idx), grid, N, corner, L, delta
				);
      });
    }



    static void initializeHaloSimulationCatalog(MarkovState& state, ptree& main_params, int cat_idx)
    {
        using PrepareDetail::ArrayDimension;
        size_t N[3];
            N[0] = static_cast<SLong&>(state["N0"]);
            N[1] = static_cast<SLong&>(state["N1"]);
            N[2] = static_cast<SLong&>(state["N2"]);
        size_t
            localN0 = static_cast<SLong&>(state["localN0"]),
            startN0 = static_cast<SLong&>(state["startN0"]);
        Console& cons = Console::instance();
        ptree& params = main_params.get_child(get_catalog_group_name(cat_idx));

        // Add a catalog in the state structure
        state.newElement(format("halo_catalog_%d") % cat_idx, new GalaxyElement(new GalaxySurveyType()));
        // Add its linear bias in the MCMC structure
        SDouble *nmean = new SDouble();
        ArrayType1d *bias = new ArrayType1d(boost::extents[0]);
        state.newElement(format("galaxy_bias_%d") % cat_idx, bias, true);
        state.newElement(format("galaxy_nmean_%d") % cat_idx, nmean, true);
        bias->setAutoResize(true);

        cons.print<LOG_DEBUG>(format("Allocating selection grid for catalog %d") % cat_idx);
        SelArrayType *sel_grid= new SelArrayType(boost::extents[range(startN0,startN0+localN0)][N[1]][N[2]]);
        cons.print<LOG_DEBUG>(format("Allocating projection grid for catalog %d") % cat_idx);
        ArrayType *data_grid = new ArrayType(boost::extents[range(startN0,startN0+localN0)][N[1]][N[2]]);
        SBool *biasRef = new SBool();

        data_grid->setRealDims(ArrayDimension(N[0], N[1], N[2]));
        sel_grid->setRealDims(ArrayDimension(N[0], N[1], N[2]));
        state.newElement(format("galaxy_bias_ref_%d") % cat_idx, biasRef);
		state.newElement(format("galaxy_sel_window_%d") % cat_idx, sel_grid);
        state.newElement(format("galaxy_data_%d") % cat_idx, data_grid);

        string halocut = to_lower_copy(params.get<string>("halo_selection", "none"));
        if (halocut == "none") {
            cons.print<LOG_DEBUG>("Apply no cut on halo catalog");
            state.newElement(format("galaxy_selection_info_%d") % cat_idx, new InfoSampleSelection());
        } else if (halocut == "mass") {
            cons.print<LOG_DEBUG>("Apply mass cuts on halo catalog");
            state.newElement(format("galaxy_selection_info_%d") % cat_idx, new InfoSampleSelection());
        } else if (halocut == "radius") {
            cons.print<LOG_DEBUG>("Apply radius cuts on halo catalog");
            state.newElement(format("galaxy_selection_info_%d") % cat_idx, new InfoSampleSelection());
        } else if (halocut == "spin") {
            cons.print<LOG_DEBUG>("Apply spin cuts on halo catalog");
            state.newElement(format("galaxy_selection_info_%d") % cat_idx, new InfoSampleSelection());
        } else if (halocut == "mixed") {
            cons.print<LOG_DEBUG>("Apply mixed cuts on halo catalog");
            state.newElement(format("galaxy_selection_info_%d") % cat_idx, new InfoSampleSelection());
        }
    }

    static void buildNoneSelectionForSimulation(GalaxySurveyType& sim, MarkovState& state, ptree& params,
            int cat_idx, CosmologicalParameters& cosmo_params, SurveyPreparer& preparer)
    {
				namespace ph = std::placeholders;
        GalaxySampleSelection& infosel = state.get<InfoSampleSelection>(format("galaxy_selection_info_%d") % cat_idx)->value;
        infosel.selector = cutterFunction(NoneCutter<GalaxySurveyType>(infosel, &sim));

        preparer = resolveHaloSurvey(state, std::bind(
            haloSimToGridGeneric< GalaxySurveyType, ArrayType::ArrayType, size_t*, double*, NoneCutter<GalaxySurveyType> >,
						 ph::_1, ph::_2, ph::_3, ph::_4, ph::_5, ph::_6,
                NoneCutter<GalaxySurveyType>(infosel, &sim) ));
    }

    static void buildMassSelectionForSimulation(GalaxySurveyType& sim, MarkovState& state, ptree& params,
            int cat_idx, CosmologicalParameters& cosmo_params, SurveyPreparer& preparer)
    {

        GalaxySampleSelection& infosel = state.get<InfoSampleSelection>(format("galaxy_selection_info_%d") % cat_idx)->value;
        infosel.low_mass_cut = params.get<double>("halo_low_mass_cut");
        infosel.high_mass_cut = params.get<double>("halo_high_mass_cut");
        infosel.selector = cutterFunction(MassCutter<GalaxySurveyType>(infosel, &sim));
				namespace ph = std::placeholders;

        preparer = resolveHaloSurvey(state, std::bind(
            haloSimToGridGeneric<
						 	GalaxySurveyType, ArrayType::ArrayType, size_t*,
							double*, MassCutter<GalaxySurveyType>
						>, ph::_1, ph::_2, ph::_3, ph::_4, ph::_5, ph::_6,
            MassCutter<GalaxySurveyType>(infosel, &sim) )
				);
    }

    static void buildRadiusSelectionForSimulation(GalaxySurveyType& sim, MarkovState& state, ptree& params,
            int cat_idx, CosmologicalParameters& cosmo_params, SurveyPreparer& preparer)
    {

        GalaxySampleSelection& infosel = state.get<InfoSampleSelection>(format("galaxy_selection_info_%d") % cat_idx)->value;
        infosel.small_radius_cut = params.get<double>("small_radius_cut");
        infosel.large_radius_cut = params.get<double>("large_radius_cut");
        infosel.selector = cutterFunction(RadiusCutter<GalaxySurveyType>(infosel, &sim));
				namespace ph = std::placeholders;

        preparer = resolveHaloSurvey(state, std::bind(
            haloSimToGridGeneric< GalaxySurveyType, ArrayType::ArrayType,
						size_t*, double*, RadiusCutter<GalaxySurveyType> >,
						ph::_1, ph::_2, ph::_3, ph::_4, ph::_5, ph::_6,
                RadiusCutter<GalaxySurveyType>(infosel, &sim) ));
    }

    static void buildSpinSelectionForSimulation(GalaxySurveyType& sim, MarkovState& state, ptree& params,
            int cat_idx, CosmologicalParameters& cosmo_params, SurveyPreparer& preparer)
    {

        GalaxySampleSelection& infosel = state.get<InfoSampleSelection>(format("galaxy_selection_info_%d") % cat_idx)->value;
		infosel.low_spin_cut = params.get<double>("halo_low_spin_cut");
		infosel.high_spin_cut = params.get<double>("halo_high_spin_cut");
		infosel.selector = cutterFunction(SpinCutter<GalaxySurveyType>(infosel, &sim));
		namespace ph = std::placeholders;

        preparer = resolveHaloSurvey(state, std::bind(
            haloSimToGridGeneric< GalaxySurveyType, ArrayType::ArrayType,
						 size_t*, double*, SpinCutter<GalaxySurveyType> >,
						 ph::_1, ph::_2, ph::_3, ph::_4, ph::_5, ph::_6,
                SpinCutter<GalaxySurveyType>(infosel, &sim) ));
    }

    static void buildMixedSelectionForSimulation(GalaxySurveyType& sim, MarkovState& state, ptree& params,
            int cat_idx, CosmologicalParameters& cosmo_params, SurveyPreparer& preparer, boost::tokenizer<>& tokenList)
    {

			namespace ph = std::placeholders;
        GalaxySampleSelection& infosel = state.get<InfoSampleSelection>(format("galaxy_selection_info_%d") % cat_idx)->value;

		MixedCutter<GalaxySurveyType> mixer;

		for (auto& tok: tokenList) {
		  if (tok == "mass") {
		    mixer.addCutter(MassCutter<GalaxySurveyType>(infosel, &sim));
			infosel.low_mass_cut = params.get<double>("halo_low_mass_cut");
			infosel.high_mass_cut = params.get<double>("halo_high_mass_cut");
		  } else if (tok == "radius") {
		    mixer.addCutter(RadiusCutter<GalaxySurveyType>(infosel, &sim));
			infosel.small_radius_cut = params.get<double>("halo_small_radius_cut");
			infosel.large_radius_cut = params.get<double>("halo_large_radius_cut");
		  } else if (tok == "spin") {
		    mixer.addCutter(SpinCutter<GalaxySurveyType>(infosel, &sim));
			infosel.low_spin_cut = params.get<double>("halo_low_spin_cut");
			infosel.high_spin_cut = params.get<double>("halo_high_spin_cut");
		  } else {
            error_helper<ErrorParams>(format("Request to cut based on %s, which is not a recognized option") % tok);
		  }

		}

        infosel.selector = cutterFunction(mixer);

        preparer = resolveHaloSurvey(state, std::bind(
            haloSimToGridGeneric< GalaxySurveyType, ArrayType::ArrayType, size_t*, double*, MixedCutter<GalaxySurveyType>>,
						ph::_1, ph::_2, ph::_3, ph::_4, ph::_5, ph::_6,
            mixer));
    }

    static void loadHaloSimulationCatalog(MarkovState& state, ptree& main_params, int cat_idx, CosmologicalParameters& cosmo_params)
    {
        ConsoleContext<LOG_INFO_SINGLE> ctx(str(format("loadHaloSimulationCatalog(%d)") % cat_idx));
        auto& sim = getHaloCatalog(state, cat_idx);
        ptree& params = main_params.get_child(get_catalog_group_name(cat_idx));
        std::string data_format = params.get<std::string>("dataformat", "TXT");

        if (data_format == "TXT") {
          loadHaloSimulationFromText(
                params.get<string>("datafile"),
                sim
          );
        } else if (data_format == "HDF5") {
          loadHaloSimulationFromHDF5(
               params.get<string>("datafile"),
               params.get<string>("datakey"),
               sim
          );
        } else if (data_format == "NONE") {
          Console::instance().print<LOG_INFO_SINGLE>("No data to be loaded");
        } else {
          error_helper<ErrorParams>(boost::format("Unknown data format '%s'") % data_format);
        }

        state.get<SBool>(format("galaxy_bias_ref_%d") % cat_idx)->value =
            params.get<bool>("refbias");

        ArrayType1d::ArrayType& hbias = *(state.get<ArrayType1d>(format("galaxy_bias_%d") % cat_idx)->array);
        if (boost::optional<std::string> bvalue = params.get_optional<std::string>("bias")) {
            auto bias_double = string_as_vector<double>(*bvalue);
            hbias.resize(boost::extents[bias_double.size()]);
            std::copy(bias_double.begin(), bias_double.end(), hbias.begin());
            string fmt_str = "Set the bias to [";
            for (int i = 0; i < hbias.size()-1; i++)
              fmt_str += "%lg,";
            fmt_str += "%lg]";
            auto fmt = boost::format(fmt_str);
            for (int i = 0; i < hbias.size()-1; i++)
              fmt = fmt % hbias[i];
            fmt = fmt % hbias[hbias.size()-1];
            ctx.print(fmt);
        } else {
            ctx.print("No initial bias value set, use bias=1");
            hbias.resize(boost::extents[1]);
            hbias[0] = 1;
        }

        double& nmean = state.get<SDouble>(format("galaxy_nmean_%d") % cat_idx)->value;
        if (boost::optional<double> nvalue = params.get_optional<double>("nmean")) {
            nmean = *nvalue;
        } else {
            ctx.print("No initial mean density value set, use nmean=1");
            nmean = 1;
        }
		}

		static void setupSimulationCatalog(MarkovState& state, ptree& main_params, int cat_idx, CosmologicalParameters& cosmo_params,
														SurveyPreparer& preparer)
		{
        ptree& params = main_params.get_child(get_catalog_group_name(cat_idx));
			  string halocut = to_lower_copy(params.get<string>("halo_selection"));
				auto& sim = getHaloCatalog(state, cat_idx);

        GalaxySelectionType& gsel_type = state.newScalar<GalaxySelectionType>(format("galaxy_selection_type_%d") % cat_idx, GALAXY_SELECTION_FILE)->value;

        if (halocut == "none") {
            gsel_type = HALO_SELECTION_NONE;
            buildNoneSelectionForSimulation(sim, state, params, cat_idx, cosmo_params, preparer);
        } else if (halocut == "mass") {
            gsel_type = HALO_SELECTION_MASS;
            buildMassSelectionForSimulation(sim, state, params, cat_idx, cosmo_params, preparer);
        } else if (halocut == "radius") {
            gsel_type = HALO_SELECTION_RADIUS;
            buildRadiusSelectionForSimulation(sim, state, params, cat_idx, cosmo_params, preparer);
        } else if (halocut == "spin") {
            gsel_type = HALO_SELECTION_SPIN;
            buildSpinSelectionForSimulation(sim, state, params, cat_idx, cosmo_params, preparer);
        } else if (halocut == "mixed") {
            gsel_type = HALO_SELECTION_MIXED;
			string cutList = to_lower_copy(params.get<string>("list_of_cuts"));
			boost::tokenizer<> tokenList(cutList);
            buildMixedSelectionForSimulation(sim, state, params, cat_idx, cosmo_params, preparer, tokenList);
        } else {
            error_helper<ErrorParams>(format("halocut has value %s, which is not recognized") % halocut);
        }
    }

    void prepareHaloSimulationData(MPI_Communication *comm, MarkovState& state, int cat_idx, CosmologicalParameters& cosmo_params,
                     SurveyPreparer const& preparer, ptree& main_params) {
        using CosmoTool::InvalidRangeException;

        size_t N[3];
            N[0] = static_cast<SLong&>(state["N0"]);
            N[1] = static_cast<SLong&>(state["N1"]);
            N[2] = static_cast<SLong&>(state["N2"]);
        size_t
            N2_HC = static_cast<SLong&>(state["N2_HC"]),
            localN0 = static_cast<SLong&>(state["localN0"]),
            startN0 = static_cast<SLong&>(state["startN0"]);
        double L[3], delta[3], corner[3];
        ConsoleContext<LOG_INFO_SINGLE> ctx("data preparation");
        Cosmology cosmo(cosmo_params);
        ptree& sys_params = main_params.get_child("system");
        ptree& g_params = main_params.get_child(get_catalog_group_name(cat_idx));

        ctx.print(format("Project data to density field grid (catalog %d)") % cat_idx);

        L[0] = static_cast<SDouble&>(state["L0"]);
        L[1] = static_cast<SDouble&>(state["L1"]);
        L[2] = static_cast<SDouble&>(state["L2"]);

        corner[0] = static_cast<SDouble&>(state["corner0"]);
        corner[1] = static_cast<SDouble&>(state["corner1"]);
        corner[2] = static_cast<SDouble&>(state["corner2"]);

        GalaxySurveyType& sim = state.get<GalaxyElement>(str(format("halo_catalog_%d") % cat_idx))->get();
        ArrayType *data_grid = state.get<ArrayType>(format("galaxy_data_%d") % cat_idx);

        delta[0] = L[0] / N[0];
        delta[1] = L[1] / N[1];
        delta[2] = L[2] / N[2];

        size_t numHalos = preparer(cat_idx, *(data_grid->array), N, corner, L, delta);
        comm->all_reduce_t(MPI_IN_PLACE, &numHalos, 1, MPI_SUM);
        if (numHalos == 0) {
          error_helper<ErrorBadState>(format("No halo at all in catalog %d") % cat_idx);
        }

        GalaxySelectionType& gsel_type = state.getScalar<GalaxySelectionType>(format("galaxy_selection_type_%d") % cat_idx);
        SelArrayType *sel_grid = state.get<SelArrayType>(format("galaxy_sel_window_%d") % cat_idx);
        LibLSS::array::fill(*sel_grid->array, 1);//fixes ambiguity when using VIRBIUS
        PrepareDetail::cleanup_data(*data_grid->array, *sel_grid->array);

    }
}

#endif
