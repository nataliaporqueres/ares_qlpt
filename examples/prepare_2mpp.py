#+
#   ARES/HADES/BORG Package -- -- ./examples/prepare_2mpp.py
#   Copyright (C) 2014-2019 Guilhem Lavaux <guilhem.lavaux@iap.fr>
#   Copyright (C) 2009-2019 Jens Jasche <jens.jasche@fysik.su.se>
#
#   Additional contributions from:
#      Guilhem Lavaux <guilhem.lavaux@iap.fr> (2015-2016, 2018)
#
#   This program is free software; you can redistribute it and/or modify it
#   under the terms of either the CeCILL license or the GNU General Public
#   license, as included with the software package.
#
#   The text of the license is located in Licence_CeCILL_V2.1-en.txt
#   and GPL.txt in the root directory of the source package.
#+
import numpy as np

LIGHTSPEED=299792.458 

tmpp = np.load("2m++.npy")

with open("2MPP.txt", mode="wt") as f:
    for i,c in enumerate(tmpp):
        f.write("%d %.15lg %.15lg %.15lg %.15lg %.15lg %.15lg\n" %
            (i,np.radians(c['ra']),np.radians(c['dec']),c['velcmb']/LIGHTSPEED,c['K2MRS'],0,c['best_velcmb']/LIGHTSPEED) )
